<?php
/**
 * Created by PhpStorm.
 * User: vasyansk
 * Date: 18/11/2018
 * Time: 14:59
 */
require 'myfuncs.php';
require 'db.conf.php';


if (!(mylogger('run cron..'))) {
    echo 'cant create log file in ./logs !)';
    die();
};

$imap = imap_open($mailserver,$mailbox,$mailpass);
 if(!($imap)) {
    mylogger("can't connect: " . imap_last_error());
    die("can't connect: " . imap_last_error());
}
$maillcheck = imap_check($imap);

//если писем нет - выходм
if (($maillcheck->Nmsgs)<1) {
    mylogger('mailbox is empty..');
    mylogger('end of cron..');
    die();
}
 //цикл по всей папке, перебор всех писем, вывод номеров, дат и адресов отправиля писем

$result = imap_fetch_overview($imap,"1:{$maillcheck->Nmsgs}",0);
$i=0;
foreach ($result as $item) {
    $header = imap_headerinfo($imap, $item->msgno);
    $fromaddr = $header->from[0]->mailbox . "@" . $header->from[0]->host;
    $fromname = $header->from[0]->mailbox;
    $subject = mydecodesubj($item->subject);
    $body = imap_body($imap,$item->msgno);

    $messageid[$i] = $item->msgno;
    $messagedate[$i] = $item->date;
    $messagesendermail[$i] = $fromaddr;
    $messagesendername[$i] = $fromname;
    $messagesubject[$i] = $subject;
    $messagebody[$i] = mb_strimwidth($body, 0, 1000, "");

    mylogger("#{$item->msgno} ({$item->date}) {$fromaddr} | тема: $subject");
//    echo "$body</br>";

    $i++;
}

//проверка на наличие отправителя в базе и создание, если его там нет
try {
    $pdo = new PDO("pgsql:host=$dbhost;dbname=$dbname", "$dblogin", "$dbpass");
}
catch(PDOException $err) {
    mylogger($err->getMessage().'</br>');
//    echo $err->getMessage().'</br>';
    die('connect to DB failed..');
}

//перебираем все письма во Входящих, дергаем каждое и запрашиваем в базе совпадение по ящику
if ($maillcheck->Nmsgs > 0) {
    for ($i = 0; $i <= ($maillcheck->Nmsgs - 1); $i++) {
        $value = $messagesendermail[$i];
        $query = "SELECT senderemail FROM sender WHERE senderemail = ?";
        $statement = $pdo->prepare($query);
        $statement->execute(array($value));
// если совпадений нет, считаем что мыло новое и добавляем данные в базу
        if (!($statement->rowCount())) {
            $query = "INSERT INTO sender (senderlogin,senderemail) VALUES (?, ?);";
            $statement = $pdo->prepare($query);
            $statement->execute(array($messagesendername[$i], $messagesendermail[$i]));
            mylogger("created new user: $value");
        }
    }
}

//создание новой задачи в базе
$i=0;
if ($maillcheck->Nmsgs > 0) {
    for ($i = 0; $i <= ($maillcheck->Nmsgs - 1); $i++) {
        //получаем senderid по мылу
        $query = "SELECT senderid FROM sender WHERE senderemail = ?";
        $statement = $pdo->prepare($query);
        $senderid = $statement->execute(array($messagesendermail[$i]));
        $senderid = $statement->fetch(PDO::FETCH_NUM);

        //создаем новый тикет
        $query = "INSERT INTO tasks (theme, senderid, userid, datecreate, statusid, body) VALUES (?, ?, 1, now(), 1, ?);";
        $statement = $pdo->prepare($query);
        $statement->execute(array($messagesubject[$i], $senderid[0], $messagebody[$i]));
    }
    mylogger("created $i tickets");
}

//перенос всех обработанных писем в подпапку Checked
if ($maillcheck->Nmsgs > 0) {
    $imapmoveresult = imap_mail_move($imap, '1:' . ($maillcheck->Nmsgs), 'Checked');
    if ($imapmoveresult == false) {
        die(imap_last_error());
    }
    if ($imapmoveresult) {
        mylogger("moved $maillcheck->Nmsgs messages to /Checked..");
        imap_expunge($imap);
    }
}

//close connections
imap_close($imap);
$pdo=null;

//запись в лог файл, когда запустился, сколько обработал
mylogger('-- end on cron.. --');