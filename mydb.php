<?php
/**
 * Copyright (c) 2018. Amega Engeneering
 */

/**
 * Created by PhpStorm.
 * User: vasyansk
 * Date: 26/11/2018
 * Time: 12:37
 */
require 'db.conf.php';

class Mydb
{
 public function get_connect($host, $login, $password, $dbname){
     try {
         $pdo = new PDO("pgsql:host=$host;dbname=$dbname", "$login", "$password");
     }
     catch(PDOException $err) {
         die('connect to DB failed..');
     }
     return $pdo;
 }
}
