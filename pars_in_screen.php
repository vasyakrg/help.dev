<?php
/**
 * Created by PhpStorm.
 * User: vasyakrg
 * Date: 18/11/2018
 * Time: 08:29
 */

//pass for mailbox: XRrvMM2athTE
$mailserver = '{imap.yandex.com:993/imap/ssl}';

$mailbox = $_POST['mailbox'];
$mailpass = $_POST['mailpass'];
$mailbodyid = $_POST['mailbodyid'];


$imap = imap_open($mailserver,$mailbox,$mailpass) or die("can't connect: " . imap_last_error());

$folders = imap_listmailbox($imap, $mailserver, "*");
$headers = imap_headers($imap);
$list    = imap_list($imap, $mailserver, "INBOX");

$MC = imap_check($imap);

//my functions
function mydecodefolder($text){
    $text = str_replace('&', '+', $text);
    $text  = str_replace(',', '/', $text);
    return iconv('UTF-7','UTF-8', $text);
}

function mydecodesubj($text){
    $val = imap_mime_header_decode($text);
    for($i=0;$i<count($val);$i++) {
        return iconv($val[$i]->charset,'UTF-8', $val[$i]->text);
    }
}

//end of my functions

//echo "Folders on server: </br>";
//    foreach ($folders as $val) {
//        echo mydecodefolder($val) . "</br>";
//    }


echo "</br>";
echo "<b>Список писем: </b></br>";

$result = imap_fetch_overview($imap,"1:{$MC->Nmsgs}",0);

foreach ($result as $overview) {
    echo "#{$overview->msgno} ({$overview->date}) - 
        </br> От: " .  mydecodesubj($overview->from) . " Тема: " . mydecodesubj($overview->subject) . " </br>";
}

echo "</br>";
if ($mailbodyid<>''){
echo "<b>текст сообщения №{$mailbodyid}: </b></br>";
echo imap_body($imap,$mailbodyid);
}

if ($_POST['mailchecked']) {
    $imapmoveresult = imap_mail_move($imap,$mailbodyid.':'.$mailbodyid,'Checked');
    if ($imapmoveresult==false){die(imap_last_error());}
    if ($imapmoveresult){
        echo "moved!";
        imap_expunge($imap);
//        header("Location: /"); //TODO хочу обновить основную страницу и выставить значения по умолчанию, перечитать index.php с нуля
    }
}

imap_close($imap);

?>
