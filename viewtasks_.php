<?php
/**
 * Copyright (c) 2018. Amega Engeneering
 */

/**
 * Created by PhpStorm.
 * User: vasyakrg
 * Date: 2018-11-30
 * Time: 11:08
 */
require 'mydb.php';


$mydb = new Mydb();
$mydb->query = 'SELECT taskid, senderlogin, theme, datecreate, statusname, userlogin, body, datecreate 
                  FROM tasks
                  INNER join sender s on tasks.senderid = s.senderid
                  INNER join statuses s2 on tasks.statusid = s2.statusid
                  INNER join users u on tasks.userid = u.userid';
$state = $mydb->get_connect($dbhost, $dblogin, $dbpass, $dbname)->query($mydb->query);

// вывожу красиво в табличку все заявки
while ($row = $state->fetch(PDO::FETCH_ASSOC)) {
    $datecreate = new DateTime($row['datecreate']);
    echo $row['taskid'] . ' ' . $row['sendername'] . ' ' . $row['theme'] . ' ' . $datecreate->format( 'Y-m-d H:i:s') . ' ' .
         $row['statusname'] . ' ' . $row['login'] . "</br>" . $row['body'] . "</br>";
    echo "</br>";
}