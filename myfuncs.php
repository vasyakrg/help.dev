<?php
/**
 * Copyright (c) 2018. Amega Engeneering
 */

/**
 * Created by PhpStorm.
 * User: vasyansk
 * Date: 19/11/2018
 * Time: 12:19
 */

function mydecodesubj($text){
    $val = imap_mime_header_decode($text);
    for($i=0;$i<count($val);$i++) {
        return trim(iconv($val[$i]->charset,'UTF-8', $val[$i]->text));
    }
}

function mylogger($text){
    $logfile = fopen('./logs/cronmail.txt', 'a+');
    $test = fwrite($logfile, date("Y-M-D H:i:s").' : '.$text.".\n");
    if ($test) {return true;} else {return false;};
    fclose($logfile);
}